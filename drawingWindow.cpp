#include "drawingWindow.h"
#include "ui_drawingWindow.h"
#include <vector>

/// \fn DrawingWindow::DrawingWindow(QWidget *parent)
/// \~English
/// \brief Constructor.
/// \param parent  when creating a DrawingWindow in the main function, leave this parameter empty
/// \~Spanish
/// \brief Constructor
/// \param parent ventana padre
DrawingWindow::DrawingWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DrawingWindow)
{
    ui->setupUi(this);

    vL = new vector<Line *>;

    // just hidding the toolbars to have a bigger drawing area.
    QList<QToolBar *> toolbars = this->findChildren<QToolBar *>();
    foreach(QToolBar *t, toolbars) t->hide();
    QList<QStatusBar *> statusbars = this->findChildren<QStatusBar *>();
    foreach(QStatusBar *t, statusbars) t->hide();
}

/// \fn void DrawingWindow::addLine(int x0, int y0, int x1, int y1, int width, QColor color)
/// \~English
/// \brief Add a line to the window, specifying coordinates
/// of the starting and end points.
/// \param x0  starting x
/// \param y0  starting y
/// \param x1  end x
/// \param y1  end y
/// \param width - line width
/// \param color - line color
/// \~Spanish
/// \brief Anade una linea a la ventana, especificando las coordenadas
/// de los puntos inciales y finales.
/// \param x0  x inicial
/// \param y0  y inicial
/// \param x1  x final
/// \param y1  y final
/// \param width  ancho de la linea
/// \param color  color de la linea
void DrawingWindow::addLine(int x0, int y0, int x1, int y1, int width, QColor color) {
    Line *tmp = new Line(x0,y0,x1,y1, width, color, this);
    tmp->show();
    vL->push_back(tmp);
}

/// \fn void DrawingWindow::addLinePolar(int x0, int y0, int length, double angle, int width, QColor color)
/// \~English
/// \brief Add a line to the window, specifying coordinates
/// of the starting point, the length and angle.
/// \param x0  starting x
/// \param y0  starting y
/// \param length  length of the line
/// \param angle  angle
/// \param width  line width
/// \param color  line color
/// \~Spanish
/// \brief Anade una linea a la ventana, especificando las coordenadas
/// del punto inicial, el largo y el angulo.
/// \param x0  x incial
/// \param y0  y inicial
/// \param length  largo de la linea
/// \param angle  angulo de la linea
/// \param width  ancho de la linea
/// \param color  color de la linea
void DrawingWindow::addLinePolar(int x0, int y0, int length, double angle, int width, QColor color) {
    Line *tmp = new Line(x0,y0,length, angle, width, color, this);
    tmp->show();
    vL->push_back(tmp);
}


/// \fn DrawingWindow::~DrawingWindow - the destructor
/// \~English
/// \brief Destructor
/// \~Spanish
/// \brief Destructor
DrawingWindow::~DrawingWindow()
{
    foreach(Line *l, *vL) delete l;
    delete vL;
    delete ui;
}


/// \fn void DrawingWindow::paintEvent(QPaintEvent *)
/// \~English
/// \brief Function called in a paint event
/// \~Spanish
/// \brief Funcion invocada en un evento de pintar
void DrawingWindow::paintEvent(QPaintEvent *) {
    QPainter p(this);
    QPen myPen;
    myPen.setWidth(1);
    myPen.setColor(QColor(0x0000ff));

    p.setPen(myPen);
    p.setBrush(Qt::cyan);

}
